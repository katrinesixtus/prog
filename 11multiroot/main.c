#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<stdlib.h>
#include<gsl/gsl_multiroots.h>


// ### First function ###
int rosenbrock(const gsl_vector *x, void *params, gsl_vector *f){
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double y0 = gsl_vector_get(f,0);
	double y1 = gsl_vector_get(f,1);
	y0 = -2*(1-x0) - 400*x0*(x1-x0*x0);
	y1 = 200*(x1-x0*x0);
	gsl_vector_set(f,0,y0);
	gsl_vector_set(f,1,y1);
	return GSL_SUCCESS;
}


// ### Second function###
double feps(double, double);

int master(const gsl_vector *x, void *params, gsl_vector *f){
	double eps = gsl_vector_get(x,0);
	double rmax = *(double*)params;
	double M = feps(rmax, eps);
	gsl_vector_set(f,0,M);
	return GSL_SUCCESS;
}

// ### main ###
// ### Part one ###

int main(){
{

const gsl_multiroot_fsolver_type *T;
gsl_multiroot_fsolver *s;

int status;
size_t iter = 0;
const size_t n = 2;

gsl_multiroot_function f = {&rosenbrock, n, NULL};

gsl_vector *x = gsl_vector_alloc(n);
// initial guesses
gsl_vector_set(x,0,2);
gsl_vector_set(x,1,5);

T = gsl_multiroot_fsolver_hybrids;
s = gsl_multiroot_fsolver_alloc (T, 2);
gsl_multiroot_fsolver_set(s,&f,x);

do{
iter++;
status = gsl_multiroot_fsolver_iterate(s);

if(status) break;

status = gsl_multiroot_test_residual(s->f,1e-6);
/*
printf ("%5ld\t%.5g\t%.5g\t%10.5g\t%10.5g\n", iter,
gsl_vector_get (s->x, 0),
gsl_vector_get (s->x, 1),
gsl_vector_get (s->f, 0),
gsl_vector_get (s->f, 1));
*/
} while(status == GSL_CONTINUE && iter < 1000);

printf("The minimum is located at (%g,%g)\n", gsl_vector_get (s->x, 0),
	gsl_vector_get (s->x, 1));

gsl_vector_free(x);
gsl_multiroot_fsolver_free(s);
}

// ### Part two ###

printf("\n\n");
printf("If one uses the more precise boundary condition, the solution is found\n");
printf("in less steps, and one can let rmax be much smaller and still find the right solution\n");
{
double rmax[] = {1,2,3,4,5,6,7,8,9,10};
for(int i = 0; i<sizeof(rmax)/sizeof(rmax[0]);i++){

printf("\n\n");
int status;
size_t iter = 0;
const size_t n = 1;

gsl_multiroot_function f;
f.f = master;
f.n = n;
f.params = (void*)&rmax[i];

gsl_vector *eps = gsl_vector_alloc(n);
// initial guesses
gsl_vector_set(eps,0,-10);

const gsl_multiroot_fsolver_type *T = gsl_multiroot_fsolver_hybrids;
gsl_multiroot_fsolver *s = gsl_multiroot_fsolver_alloc (T, n);
gsl_multiroot_fsolver_set(s,&f,eps);


do{
	iter++;
	status = gsl_multiroot_fsolver_iterate(s);
	if(status) break;

status = gsl_multiroot_test_residual(s->f,1e-6);
printf ("%5ld\t%.5g\t%10.5g\n", iter,
	gsl_vector_get (s->x, 0),
	gsl_vector_get (s->f, 0));
} while(status == GSL_CONTINUE && iter < 1000);

fprintf(stderr, "%g\t%g\n", gsl_vector_get (s->x, 0), gsl_vector_get (s->f, 0));

gsl_vector_free(eps);
gsl_multiroot_fsolver_free(s);

}
}
return 0;
}
