#include<math.h>
#include<assert.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int hydro_s_diff(double r, const double f[], double dfdr[], void* params) {
	double eps = *(double*)params;
	dfdr[0] = f[1];
	dfdr[1] = -2*(eps+1/r)*f[0];
return GSL_SUCCESS;
}

double feps(double r, double eps){
	double rmin = 1e-3;
	if(r<=rmin) return r-r*r;
	gsl_odeiv2_system sys;
	sys.function = hydro_s_diff;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (void*)&eps;

double step = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
gsl_odeiv2_driver* driver =
gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rk8pd, step, epsabs, epsrel);

double F[2] = {rmin-rmin*rmin , 1-2*rmin};
int status;
double t = rmin;
status = gsl_odeiv2_driver_apply(driver, &t, r, F);
gsl_odeiv2_driver_free(driver);

if(status != GSL_SUCCESS)
	fprintf(stderr, "The driver failed to evolve system. Last F=%g\n", F[0]);
	return F[0];

}
