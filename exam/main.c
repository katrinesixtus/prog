#include<stdio.h>
#include<math.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_vector.h>
#include<assert.h>

double function(const gsl_vector* v, void*params){
double x = gsl_vector_get(v,0);
double f = (x*x-0.5*x)/2; 
return f;
}

int main(){
{
FILE* data = fopen("data.txt","w");
size_t iter = 0;
int status;
double size;

gsl_vector *x = gsl_vector_alloc(1);
gsl_vector_set(x,0,0.1);

int dim = 1;
gsl_multimin_function fun;
fun.f = &function;
fun.n = dim;
fun.params = NULL;

gsl_multimin_fminimizer *s = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2,dim);
gsl_vector *step = gsl_vector_alloc(1);
gsl_vector_set(step,0,0.1);
gsl_multimin_fminimizer_set(s,&fun,x,step);

do {
iter++;
status = gsl_multimin_fminimizer_iterate (s);
printf("%i, %g\n", status, gsl_vector_get(s->x,0));
if(status) break;
size = gsl_multimin_fminimizer_size (s);
status = gsl_multimin_test_size (size, 1e-6);

} while(status == GSL_CONTINUE && iter < 1000);
fprintf (data,"%.5g \t %10.5g\n",gsl_vector_get (s->x, 0),s->fval);

printf("The minimum is at the x-value (%g).\n",gsl_vector_get (s->x, 0));

gsl_multimin_fminimizer_free(s);
gsl_vector_free(x);

fclose(data);

double q = 0.005;
double r = 0.0;
double q_step = 0.005;
size_t it = 0;
int maxstep = 200;
FILE *data2 = fopen("data2.txt","w");
for(it; it < maxstep; it++){
r = (q*q-0.5*q)/2;
fprintf(data2,"%.5g \t %10.5g \n",q,r);
q += q_step;
}

fclose(data2);

}
return 0;
}
