#include<stdio.h>
#include<getopt.h>
#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>

int diff_equation (double x, const double y[], double yprime[], void *params) {
	(void) x;
	yprime[0] = y[0]*(1-y[0]);
	return GSL_SUCCESS;
}

int orbit_func (double phi, const double y[], double yprime[], void *params){
	(void) phi;
	double epsilon = *(double *) params;
	yprime[0] = y[1];
	yprime[1] = 1 - y[0] + epsilon * y[0] * y[0];
	return GSL_SUCCESS;
}

int main(){
{
	double t = 0.0;
	int dimension = 1;
	double xmin = 0,xmax = 3,dx = (xmax-xmin)/20;
	double hstart = 1e-3, epsabs =1e-6, epsrel = 1e-6;
	gsl_odeiv2_system diff = {diff_equation, NULL, dimension, NULL};
	gsl_odeiv2_driver *driver = gsl_odeiv2_driver_alloc_y_new (
	&diff,gsl_odeiv2_step_rk8pd,hstart,epsabs,epsrel);
	double y[1] = {0.5};

	for(double x=xmin;x<=xmax;x+=dx){
		int status = gsl_odeiv2_driver_apply(driver,&t,x,y);
		printf("%g %g %g\n",x,y[0],1/(1+exp(-x)));
			if (status != GSL_SUCCESS) fprintf (stderr, "fun: status=%i", status);
}
gsl_odeiv2_driver_free(driver);}

{
	printf("\n\n");
	double t=0.0;
	double hstart = 1e-3, epsabs =1e-6 , epsrel =1e-6 ;
	double params[3];
	double x;
	for(int i = 0; i<3; i++){
		scanf("%lg",&x);
		params[i]=x;}

	int dimension = 2;
	double epsilon = params[0], u[2] = {params[1],params[2]};
	double phimin = 0, phimax = 39.5*10 * M_PI, dphi = 0.05;
	gsl_odeiv2_system orbit = {orbit_func, NULL, dimension, &epsilon};
	gsl_odeiv2_driver *driver_orbit = gsl_odeiv2_driver_alloc_y_new(
	&orbit,gsl_odeiv2_step_rk8pd,hstart,epsabs,epsrel);

	for(double phi = phimin; phi<phimax;phi+=dphi){
		int status = gsl_odeiv2_driver_apply(driver_orbit,&t,phi,u);
		printf("%g %g\n",phi,u[0]);
			if (status != GSL_SUCCESS) fprintf (stderr, "fun: status=%i", status);
}
gsl_odeiv2_driver_free(driver_orbit);}

return 0;
}
