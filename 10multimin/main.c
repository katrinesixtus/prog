#include<stdio.h>
#include<math.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_errno.h>
#include<assert.h>


//### Rosenbrock function ###
struct data_type{int n; double *t, *y, *e;};

double rosenbrock_fun(const gsl_vector* v, void* params){
double x, y;
x = gsl_vector_get(v,0);
y = gsl_vector_get(v,1);
double f = pow((1 - x),2) + 100*pow((y - pow(x,2)),2);
return f;
}

//### Least squares fit ###

double minimizer_function (const gsl_vector *x, void *params){
	struct data_type* par = (struct data_type*)params;
	int n = par->n;
	double *t = par->t;
	double *y = par->y;
	double *e = par->e;
	double sum = 0;
	double A = gsl_vector_get(x,0);
	double B = gsl_vector_get(x,1);
	double T = gsl_vector_get(x,2);
	#define f(t) A*exp(-(t)/T)+B
	for(int i=0; i<n; i++) sum+=pow(( f(t[i]) - y[i])/e[i] ,2);
	return sum;
}


int main(){

//### Part one ###
{
FILE* PartA = fopen("PartA.log","w");
gsl_multimin_function funcA;
funcA.f = rosenbrock_fun;
funcA.n = 2;
funcA.params = NULL;

double size, size_demand = 1e-3;
int iter = 0;
int status;
gsl_vector* x_start= gsl_vector_alloc(2);
gsl_vector_set(x_start,0,-2);
gsl_vector_set(x_start,1,-2);

gsl_vector* step_size = gsl_vector_alloc(2);
gsl_vector_set_all(step_size,1);

gsl_multimin_fminimizer* multimin_workspace = gsl_multimin_fminimizer_alloc(
gsl_multimin_fminimizer_nmsimplex2,funcA.n);

gsl_multimin_fminimizer_set(multimin_workspace,&funcA,x_start,step_size);

fprintf(PartA,"Iteration x y f_min size\n");
do
{
iter++;
status = gsl_multimin_fminimizer_iterate(multimin_workspace);

if (status) {break;}

size = gsl_multimin_fminimizer_size (multimin_workspace);
status = gsl_multimin_test_size (size, size_demand);

if (status == GSL_SUCCESS) {
	printf("\nMinimum found at:\n");
	printf("Iteration x y f_min size\n");
	printf ("%5d %.5f %.5f %10.5f %.5f\n", iter,
	gsl_vector_get(gsl_multimin_fminimizer_x(multimin_workspace),0),
	gsl_vector_get(gsl_multimin_fminimizer_x(multimin_workspace),1),
	gsl_multimin_fminimizer_minimum(multimin_workspace),
	gsl_multimin_fminimizer_size(multimin_workspace));}

fprintf (PartA,"%5d %.5f %.5f %10.5f %.5f\n", iter,
	gsl_vector_get(gsl_multimin_fminimizer_x(multimin_workspace),0),
	gsl_vector_get(gsl_multimin_fminimizer_x(multimin_workspace),1),
	gsl_multimin_fminimizer_minimum(multimin_workspace),
	gsl_multimin_fminimizer_size(multimin_workspace));

} while(status == GSL_CONTINUE && iter < 1000);

gsl_multimin_fminimizer_free(multimin_workspace);
gsl_vector_free(x_start);
gsl_vector_free(step_size);
}


//### Part two ###

{
FILE* PartB = fopen("PartB.data","w");

double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};

int n=sizeof(t)/sizeof(t[0]);

struct data_type par = {n,t,y,e};

gsl_vector *x = gsl_vector_alloc(3);
gsl_vector_set_all(x,1);

gsl_vector *step_size = gsl_vector_alloc(3);
gsl_vector_set_all(step_size,0.01);

double size, size_demand = 1e-4;
int status, iter = 0;;

gsl_multimin_function fB;
fB.f=minimizer_function;
fB.n=3;
fB.params=&par;

gsl_multimin_fminimizer *s = gsl_multimin_fminimizer_alloc(
gsl_multimin_fminimizer_nmsimplex2,fB.n);

gsl_multimin_fminimizer_set(s,&fB,x,step_size);

double A,B,T;


do {
iter++;
status = gsl_multimin_fminimizer_iterate(s);
if(status) break;
	size = gsl_multimin_fminimizer_size (s);
	status = gsl_multimin_test_size (size, size_demand);
if(status == GSL_SUCCESS){
	A = gsl_vector_get(gsl_multimin_fminimizer_x(s),0);
	B = gsl_vector_get(gsl_multimin_fminimizer_x(s),1);
	T = gsl_vector_get(gsl_multimin_fminimizer_x(s),2);
	printf("\nFound Minimum for funcB.\nIteration A B T\n");
	printf("%5d %3.5g %3.5g %3.5g\n",iter,A,B,T);
}
} while(status == GSL_CONTINUE && iter < 1000);

#define f_min(t,A,B,T) A*exp(-(t)/T)+B
fprintf(PartB,"t\tf(x)\n");
double t_max = 10, t_delta = 0.1, tid=0;
for (int i = 0; i < t_max/t_delta; i++) {
	tid +=t_delta;
	fprintf(PartB,"%g\t%g\n",tid,f_min(tid,A,B,T));
	}
	fprintf(PartB,"\n\n");
for(int i = 0; i<n; i++){
	fprintf(PartB,"%g\t%g\t%g\n",t[i],y[i],e[i]);
}

gsl_multimin_fminimizer_free(s);
gsl_vector_free(x);
gsl_vector_free(step_size);
}

return 0;
}
